// Lesson 3
function fizzBuzz() {
  for (let i = 1; i < 100; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
      console.log("FizzBuzz");
    } else if (i % 3 == 0) {
      console.log("Fizz");
    } else if (i % 5 == 0) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
}

function filterArray(arr) {
  arr = arr
    .filter(function (item) {
      return Array.isArray(item);
    })
    .reduce(function (acc, curr) {
      return [...acc, ...curr];
    })
    .sort(function (a, b) {
      return a - b;
    });
  return Array.from(new Set(arr));
}

// Output DON'T TOUCH!
console.log(fizzBuzz());
console.log(filterArray([[2], 23, "dance", true, [3, 5, 3]]));
